﻿using Newtonsoft.Json.Linq;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using VersionControllerModule.Factories;
using VersionControllerModule.Models;
using VersionControllerModule.Services;
using VersionControllerModule.Translations;
using System.ComponentModel;

namespace VersionControllerModule.Controllers
{
    public class VersioningController : VersioningViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ServiceAgent_Elastic serviceAgentElastic;
        private VersionFactory versionFactory;
        private OntoWebCore.Factories.ComboboxFactory<LogStateItem> comboboxFactory;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;

        private clsOntologyItem refItem;

        private VersionItem currentVersion;

        private List<LogStateItem> logStateItems;

        private string dedicatedSender;
        private string selectedLogStateId;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public VersioningController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += VersionIngController_PropertyChanged;
        }

        private void VersionIngController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));
            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            CheckClear();

            
        }

        private void CheckClear()
        {
            if (currentVersion != null)
            {
                IsEnabled_Clear = Number_Major != currentVersion.Major ||
                    Number_Minor != currentVersion.Minor ||
                    Number_Build != currentVersion.Build ||
                    Number_Revision != currentVersion.Revision ||
                    currentVersion.ID_Logstate != selectedLogStateId ||
                    currentVersion.Message != Text_Description;

               
                
            }
            else
            {
                IsEnabled_Clear = false;
            }
            
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            comboboxFactory = new ComboboxFactory<LogStateItem>();
            versionFactory = new VersionFactory(localConfig);
            versionFactory.PropertyChanged += VersionFactory_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic.StopRead();
            serviceAgentElastic = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            Readonly_Description = true;

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            dedicatedSender = webSocketServiceAgent.Context.QueryString["Sender"];


            webSocketServiceAgent.SetEnable(false);
            webSocketServiceAgent.SetVisibility(true);
            webSocketServiceAgent.SendModel();
        }

        private void StateMachine_loginSucceded()
        {
           
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
           

            serviceAgentElastic.GetLogStates();
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void VersionFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.VersionFactory_ResultVersions)
            {
                if (versionFactory.ResultVersions.GUID == localConfig.Globals.LState_Success.GUID ||
                    versionFactory.ResultVersions.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    currentVersion = versionFactory.Versions.FirstOrDefault();

                    if (currentVersion != null)
                    {
                        Number_Major = currentVersion.Major.Value;
                        Number_Minor = currentVersion.Minor.Value;
                        Number_Build = currentVersion.Build.Value;
                        Number_Revision = currentVersion.Revision.Value;

                        
                    }
                    else
                    {
                        Number_Major = Number_Minor = Number_Build = Number_Revision = 0;
                    }

                    CheckControlStates(true);

                    SetLogState();
                }
                else
                {
                    CheckControlStates(false);
                    SetLogState();
                }
            }
        }

        private void CheckControlStates(bool isActive)
        {
            if (isActive)
            {
                Readonly_Description = true;
                IsEnabled_Major = true;
                IsEnabled_Minor = true;
                IsEnabled_Build = true;
                IsEnabled_Revision = true;
                IsEnabled_Ok = true;
                IsEnabled_LogStates = true;
                IsEnabled_Clear = false;
            }
            else
            {
                Readonly_Description = true;
                IsEnabled_Major = false;
                IsEnabled_Minor = false;
                IsEnabled_Build = false;
                IsEnabled_Revision = false;
                IsEnabled_Ok = true;
                IsEnabled_Clear = false;
                IsEnabled_LogStates = false;
                Text_Description = "";
            }
        }

        private void SetLogState()
        {
            if (logStateItems == null || currentVersion == null) return;
            if (logStateItems != null && currentVersion != null)
            {
                Text_Description = currentVersion.Message;
                var logState = logStateItems.FirstOrDefault(usr => usr.IdLogState == currentVersion.ID_Logstate);
                var ix = logStateItems.IndexOf(logState);
                Index_SelectedLogStateId = ix;
            }
            else if (logStateItems != null)
            {
                Text_Description = "";
                var logState = logStateItems.FirstOrDefault(usr => usr.IdLogState == localConfig.OItem_token_logstate_information.GUID);
                var ix = logStateItems.IndexOf(logState);
                Index_SelectedLogStateId = ix;
            }
            
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ServiceAgentElastic_ResultVersions)
            {
                versionFactory.FinishCurrentVersion(refItem,
                        serviceAgentElastic.CurrentVersion);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ServiceAgentElastic_LogStates)
            {
                if (serviceAgentElastic.LogStates != null)
                {
                    logStateItems = versionFactory.GetLogstateItems(serviceAgentElastic.LogStates).OrderBy(logStateItm => logStateItm.NameLogState).ToList();

                    var fileName = Guid.NewGuid().ToString() + ".json";
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);

                    JqxDataSource_LogStates = comboboxFactory.CreateDataSource(logStateItems.ToList<object>(), sessionFile);

                    SetLogState();
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "SelectedLogstate")
                {
                    selectedLogStateId = webSocketServiceAgent.Request["Id"].ToString();
                    var logState = logStateItems.FirstOrDefault(usr => usr.IdLogState == selectedLogStateId);
                    var ix = logStateItems.IndexOf(logState);
                    Index_SelectedLogStateId = ix;
                    CheckClear();

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "Clear")
                {
                    Clear();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SaveVersion")
                {

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "ToggleReadonlyOfDescription")
                {
                    Readonly_Description = !Readonly_Description;
                    
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                GetChangedViewItems();
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                if (webSocketServiceAgent.ObjectArgument == null) return;
                var oItemObject = serviceAgentElastic.GetOItem(webSocketServiceAgent.ObjectArgument.Value, localConfig.Globals.Type_Object);

                if (oItemObject == null) return;
                SelectedOItem(oItemObject);
            }
            

        }

        private void Clear()
        {
            if (currentVersion != null)
            {
                Number_Build = currentVersion.Build.Value;
                Number_Major = currentVersion.Major.Value;
                Number_Minor = currentVersion.Minor.Value;
                Number_Revision = currentVersion.Revision.Value;

                SetLogState();
            }
            else
            {
                Number_Build = 0;
                Number_Major = 0;
                Number_Minor = 0;
                Number_Revision = 0;

                SetLogState();
            }

            CheckClear();
        }

        private void GetChangedViewItems()
        {
            webSocketServiceAgent.ChangedViewItems.ForEach(changeItem =>
            {
               

            });

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                var oItem = message.OItems.FirstOrDefault();
                if (oItem == null) return;

                SelectedOItem(oItem);

            }
        }

        private void SelectedOItem(clsOntologyItem oItem)
        {
            
            if (refItem != null && refItem.GUID == oItem.GUID) return;
            selectedLogStateId = "";
            refItem = oItem;

            serviceAgentElastic.GetCurrentVersion(refItem);
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
