﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using VersionControllerModule.Factories;
using VersionControllerModule.Models;
using VersionControllerModule.Services;
using VersionControllerModule.Translations;
using System.ComponentModel;

namespace VersionControllerModule.Controllers
{
    public class VersionListController : VersionListViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ServiceAgent_Elastic serviceAgentElastic;
        private VersionFactory versionFactory;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;

        private string fileNameVersionGrid;

        private clsOntologyItem refItem;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public VersionListController()
        {

            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += VersionController_PropertyChanged;
        }

        private void VersionController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));
            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            versionFactory = new VersionFactory(localConfig);
            versionFactory.PropertyChanged += VersionFactory_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.SetEnable(false);
            webSocketServiceAgent.SetVisibility(true);
            webSocketServiceAgent.SendModel();
        }

        private void StateMachine_loginSucceded()
        {
            

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            fileNameVersionGrid = Guid.NewGuid().ToString() + ".json";

            var sessionVersionGrid = webSocketServiceAgent.RequestWriteStream(fileNameVersionGrid);

            var passwordList = new List<VersionItem>();

            using (sessionVersionGrid.StreamWriter)
            {
                using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(sessionVersionGrid.StreamWriter))
                {
                    jsonTextWriter.WriteRaw(Newtonsoft.Json.JsonConvert.SerializeObject(passwordList));
                }
            }

            ColumnConfig_Grid = GridFactory.CreateColumnList(typeof(VersionItem));
            JqxDataSource_Grid = GridFactory.CreateJqxDataSource(typeof(VersionItem), 0, 20, sessionVersionGrid.FileUri.AbsoluteUri);
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void VersionFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.VersionFactory_ResultVersions)
            {
                if (versionFactory.ResultVersions.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var sessionVersionGrid = webSocketServiceAgent.RequestWriteStream(fileNameVersionGrid);

                    using (sessionVersionGrid.StreamWriter)
                    {
                        using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(sessionVersionGrid.StreamWriter))
                        {
                            jsonTextWriter.WriteRaw(Newtonsoft.Json.JsonConvert.SerializeObject(versionFactory.Versions.OrderByDescending(versionItem => versionItem.OrderID)));
                        }
                    }

                    Command_Grid = "Refresh";
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ServiceAgentElastic_ResultVersions)
            {
                if (serviceAgentElastic.ResultVersions.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    versionFactory.GetVersions(refItem,
                        serviceAgentElastic.VersionsOfRef,
                        serviceAgentElastic.OItemDirection,
                        serviceAgentElastic.MajorNumbers,
                        serviceAgentElastic.MinorNumbers,
                        serviceAgentElastic.BuildNumbers,
                        serviceAgentElastic.RevisionNumbers);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList )
            {
                var oItem = message.OItems.FirstOrDefault();
                if (oItem == null) return;
                if (refItem != null && refItem.GUID == oItem.GUID) return;

                refItem = oItem;

                IsEnabled_NewItem = true;
                serviceAgentElastic.GetVersions(refItem);

            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
