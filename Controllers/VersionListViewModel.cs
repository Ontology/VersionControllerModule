﻿using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VersionControllerModule.Notifications;

namespace VersionControllerModule.Controllers
{
    public class VersionListViewModel : ViewModelBase
    {
        private bool issuccessful_Login;

        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private JqxColumnList columnconfig_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.ColumnConfig)]
        public JqxColumnList ColumnConfig_Grid
        {
            get { return columnconfig_Grid; }
            set
            {
                if (columnconfig_Grid == value) return;

                columnconfig_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnConfig_Grid);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private string command_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.Command)]
        public string Command_Grid
        {
            get { return command_Grid; }
            set
            {
                if (command_Grid == value) return;

                command_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Command_Grid);

            }
        }

        private JqxDataSource jqxdatasource_ContextMenu;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenu, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Content)]
        public JqxDataSource JqxDataSource_ContextMenu
        {
            get { return jqxdatasource_ContextMenu; }
            set
            {
                if (jqxdatasource_ContextMenu == value) return;

                jqxdatasource_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_ContextMenu);

            }
        }

        private bool isvisible_ContextMenu;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenu, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_ContextMenu
        {
            get { return isvisible_ContextMenu; }
            set
            {
                if (isvisible_ContextMenu == value) return;

                isvisible_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ContextMenu);

            }
        }

        private bool isenabled_ContextMenu;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenu, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ContextMenu
        {
            get { return isenabled_ContextMenu; }
            set
            {
                if (isenabled_ContextMenu == value) return;

                isenabled_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ContextMenu);

            }
        }

        private bool isvisible_RemoveItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "removeItem", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_RemoveItem
        {
            get { return isvisible_RemoveItem; }
            set
            {
                if (isvisible_RemoveItem == value) return;

                isvisible_RemoveItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_RemoveItem);

            }
        }

        private bool isenabled_RemoveItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "removeItem", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_RemoveItem
        {
            get { return isenabled_RemoveItem; }
            set
            {
                if (isenabled_RemoveItem == value) return;

                isenabled_RemoveItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_RemoveItem);

            }
        }

        private string label_RemoveItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "removeItem", ViewItemType = ViewItemType.Content)]
        public string Label_RemoveItem
        {
            get { return label_RemoveItem; }
            set
            {
                if (label_RemoveItem == value) return;

                label_RemoveItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_RemoveItem);

            }
        }

        private bool isenabled_NewItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "newItem", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_NewItem
        {
            get { return isenabled_NewItem; }
            set
            {
                if (isenabled_NewItem == value) return;

                isenabled_NewItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NewItem);

            }
        }

        private bool isvisible_NewItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "newItem", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_NewItem
        {
            get { return isvisible_NewItem; }
            set
            {
                if (isvisible_NewItem == value) return;

                isvisible_NewItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_NewItem);

            }
        }

        private string label_NewItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "newItem", ViewItemType = ViewItemType.Content)]
        public string Label_NewItem
        {
            get { return label_NewItem; }
            set
            {
                if (label_NewItem == value) return;

                label_NewItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_NewItem);

            }
        }
    }
}
