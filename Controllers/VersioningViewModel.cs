﻿using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VersionControllerModule.Models;
using VersionControllerModule.Notifications;

namespace VersionControllerModule.Controllers
{
    public class VersioningViewModel : ViewModelBase
    {
        private bool issuccessful_Login;

        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string label_Major;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "majorLbl", ViewItemType = ViewItemType.Content)]
        public string Label_Major
        {
            get { return label_Major; }
            set
            {
                if (label_Major == value) return;

                label_Major = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Major);

            }
        }

        private string label_Minor;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "minorLbl", ViewItemType = ViewItemType.Content)]
        public string Label_Minor
        {
            get { return label_Minor; }
            set
            {
                if (label_Minor == value) return;

                label_Minor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Minor);

            }
        }

        private string label_Build;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "buildLbl", ViewItemType = ViewItemType.Content)]
        public string Label_Build
        {
            get { return label_Build; }
            set
            {
                if (label_Build == value) return;

                label_Build = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Build);

            }
        }

        private string label_Revision;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "revisionLbl", ViewItemType = ViewItemType.Content)]
        public string Label_Revision
        {
            get { return label_Revision; }
            set
            {
                if (label_Revision == value) return;

                label_Revision = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Revision);

            }
        }

        private bool isenabled_Major;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "majorInput", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Major
        {
            get { return isenabled_Major; }
            set
            {
                if (isenabled_Major == value) return;

                isenabled_Major = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Major);

            }
        }

        private long number_Major;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "majorInput", ViewItemType = ViewItemType.Content)]
        public long Number_Major
        {
            get { return number_Major; }
            set
            {
                if (number_Major == value) return;

                number_Major = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Number_Major);

            }
        }

        private bool isenabled_Minor;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "minorInput", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Minor
        {
            get { return isenabled_Minor; }
            set
            {
                if (isenabled_Minor == value) return;

                isenabled_Minor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Minor);

            }
        }

        private long number_Minor;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "minorInput", ViewItemType = ViewItemType.Content)]
        public long Number_Minor
        {
            get { return number_Minor; }
            set
            {
                if (number_Minor == value) return;

                number_Minor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Number_Minor);

            }
        }

        private bool isenabled_Build;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "buildInput", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Build
        {
            get { return isenabled_Build; }
            set
            {
                if (isenabled_Build == value) return;

                isenabled_Build = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Build);

            }
        }

        private long number_Build;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "buildInput", ViewItemType = ViewItemType.Content)]
        public long Number_Build
        {
            get { return number_Build; }
            set
            {
                if (number_Build == value) return;

                number_Build = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Number_Build);

            }
        }

        private bool isenabled_Revision;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "revisionInput", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Revision
        {
            get { return isenabled_Revision; }
            set
            {
                if (isenabled_Revision == value) return;

                isenabled_Revision = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Revision);

            }
        }

        private long number_Revision;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "revisionInput", ViewItemType = ViewItemType.Content)]
        public long Number_Revision
        {
            get { return number_Revision; }
            set
            {
                if (number_Revision == value) return;

                number_Revision = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Number_Revision);

            }
        }

        private bool isenabled_Ok;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "ok", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Ok
        {
            get { return isenabled_Ok; }
            set
            {
                if (isenabled_Ok == value) return;

                isenabled_Ok = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Ok);

            }
        }

        private string label_Ok;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "ok", ViewItemType = ViewItemType.Content)]
        public string Label_Ok
        {
            get { return label_Ok; }
            set
            {
                if (label_Ok == value) return;

                label_Ok = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Ok);

            }
        }

        private bool isenabled_Clear;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "clear", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Clear
        {
            get { return isenabled_Clear; }
            set
            {
                if (isenabled_Clear == value) return;

                isenabled_Clear = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Clear);

            }
        }

        private string label_Clear;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "clear", ViewItemType = ViewItemType.Content)]
        public string Label_Clear
        {
            get { return label_Clear; }
            set
            {
                if (label_Clear == value) return;

                label_Clear = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Clear);

            }
        }

        private bool readonly_Description;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.TextArea, ViewItemId = "inpDescription", ViewItemType = ViewItemType.Readonly)]
        public bool Readonly_Description
        {
            get { return readonly_Description; }
            set
            {

                readonly_Description = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Readonly_Description);

            }
        }

        private string text_Description;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.TextArea, ViewItemId = "inpDescription", ViewItemType = ViewItemType.Content)]
        public string Text_Description
        {
            get { return text_Description; }
            set
            {
                if (text_Description == value) return;

                text_Description = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Description);

            }
        }

        private bool isenabled_LogStates;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboLogState", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_LogStates
        {
            get { return isenabled_LogStates; }
            set
            {

                isenabled_LogStates = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_LogStates);

            }
        }

        private JqxDataSource jqxdatasource_LogStates;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboLogState", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_LogStates
        {
            get { return jqxdatasource_LogStates; }
            set
            {
                if (jqxdatasource_LogStates == value) return;

                jqxdatasource_LogStates = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_LogStates);

            }
        }

        private int index_SelectedLogStateId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Combobox, ViewItemId = "cmboLogState", ViewItemType = ViewItemType.SelectedIndex)]
        public int Index_SelectedLogStateId
        {
            get { return index_SelectedLogStateId; }
            set
            {
                if (index_SelectedLogStateId == value) return;

                index_SelectedLogStateId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Index_SelectedLogStateId);

            }
        }

        internal VersionItem versionItem;

    }
}
