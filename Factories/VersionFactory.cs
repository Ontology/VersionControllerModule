﻿using LoggingServiceController;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VersionControllerModule.Models;
using VersionControllerModule.Notifications;

namespace VersionControllerModule.Factories
{
    public class VersionFactory : NotifyPropertyChange
    {
        public clsLocalConfig localConfig;

        private LoggingManager loggingManager;

        private clsOntologyItem oItemRef;
        private List<clsObjectRel> versionsOfRef;
        private clsOntologyItem oItemDirection;
        private List<clsObjectAtt> majorList;
        private List<clsObjectAtt> minorList;
        private List<clsObjectAtt> buildList;
        private List<clsObjectAtt> revisionList;

        private VersionItem currentVersion;
        public List<VersionItem> Versions { get; set; }

        private bool currentVersionMode;

        private clsOntologyItem resultVersions;
        public clsOntologyItem ResultVersions
        {
            get { return resultVersions; }
            set
            {
                resultVersions = value;
                RaisePropertyChanged(NotifyChanges.VersionFactory_ResultVersions);
            }
        }

        public clsOntologyItem FinishCurrentVersion(clsOntologyItem oItemRef, 
                                                    VersionItem currentVersion)
        {
            currentVersionMode = true;
            this.oItemRef = oItemRef;
            this.currentVersion = currentVersion;
            if (currentVersion == null) return localConfig.Globals.LState_Nothing.Clone();

            var versionItems = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = currentVersion.ID_Version,
                    Name = currentVersion.Name_Version,
                    GUID_Parent = localConfig.OItem_type_developmentversion.GUID,
                    Type = localConfig.Globals.Type_Object
                }
            };

            var result = loggingManager.GetRelatedLogEntries(versionItems);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultVersions = result;
            }

            return localConfig.Globals.LState_Success.Clone();
        }


        public List<LogStateItem> GetLogstateItems(List<clsOntologyItem> logStates)
        {
            return logStates.Select(logState => new LogStateItem
            {
                IdLogState = logState.GUID,
                NameLogState = logState.Name
            }).ToList();
        }

        public clsOntologyItem GetVersions(clsOntologyItem oItemRef,
                                        List<clsObjectRel> versionsOfRef, 
                                        clsOntologyItem oItemDirection, 
                                        List<clsObjectAtt> majorList,
                                        List<clsObjectAtt> minorList,
                                        List<clsObjectAtt> buildList,
                                        List<clsObjectAtt> revisionList)
        {
            currentVersionMode = false;
            this.oItemRef = oItemRef;
            this.versionsOfRef = versionsOfRef;
            this.oItemDirection = oItemDirection;
            this.majorList = majorList;
            this.minorList = minorList;
            this.buildList = buildList;
            this.revisionList = revisionList;

            List<clsOntologyItem> versionItems;

            if (oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                versionItems = versionsOfRef.Select(versionOfRef => new clsOntologyItem
                {
                    GUID = versionOfRef.ID_Other,
                    Name = versionOfRef.Name_Other,
                    GUID_Parent = versionOfRef.ID_Parent_Other,
                    Type = localConfig.Globals.Type_Object
                }).ToList();
            }
            else
            {
                versionItems = versionsOfRef.Select(versionOfRef => new clsOntologyItem
                {
                    GUID = versionOfRef.ID_Object,
                    Name = versionOfRef.Name_Object,
                    GUID_Parent = versionOfRef.ID_Parent_Object,
                    Type = localConfig.Globals.Type_Object
                }).ToList();
            }

            var result = loggingManager.GetRelatedLogEntries(versionItems);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultVersions = result;
            }

            return localConfig.Globals.LState_Success.Clone();
        }

        public VersionFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            loggingManager = new LoggingManager(localConfig.Globals);
            loggingManager.PropertyChanged += LoggingManager_PropertyChanged;
        }

        private void LoggingManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.Elastic_ResultData)
            {
                if (loggingManager.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    ResultVersions = localConfig.Globals.LState_Error.Clone();
                }
                else if (loggingManager.ResultData.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (!currentVersionMode)
                    {
                        if (oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                        {

                            Versions = (from versionRef in versionsOfRef
                                        join major in majorList on versionRef.ID_Other equals major.ID_Object
                                        join minor in minorList on versionRef.ID_Other equals minor.ID_Object
                                        join build in buildList on versionRef.ID_Other equals build.ID_Object
                                        join revision in revisionList on versionRef.ID_Other equals revision.ID_Object
                                        join logEntry in loggingManager.LogEntries on versionRef.ID_Other equals logEntry.IdRef
                                        select new VersionItem
                                        {
                                            ID_Version = versionRef.ID_Other,
                                            Name_Version = versionRef.Name_Other,
                                            ID_Logentry = logEntry.IdLogEntry,
                                            Name_Logentry = logEntry.NameLogEntry,
                                            ID_Logstate = logEntry.IdLogState,
                                            Name_Logstate = logEntry.NameLogState,
                                            DateTimeStamp = logEntry.DateTimeStamp,
                                            ID_Attribute_DateTimeStamp = logEntry.IdAttributeDateTimeStamp,
                                            Message = logEntry.Message,
                                            ID_Attribute_Message = logEntry.IdAttributeMessage,
                                            ID_User = logEntry.IdUser,
                                            Name_User = logEntry.NameUser,
                                            ID_Attribute_Major = major.ID_Attribute,
                                            Major = major.Val_Lng,
                                            ID_Attribute_Minor = minor.ID_Attribute,
                                            Minor = minor.Val_Lng,
                                            ID_Attribute_Build = build.ID_Attribute,
                                            Build = build.Val_Lng,
                                            ID_Attribute_Revision = revision.ID_Attribute,
                                            Revision = revision.Val_Lng,
                                            OrderID = versionRef.OrderID
                                        }).ToList();
                            ResultVersions = localConfig.Globals.LState_Success.Clone();
                        }
                        else
                        {
                            Versions = (from versionRef in versionsOfRef
                                        join major in majorList on versionRef.ID_Object equals major.ID_Object
                                        join minor in minorList on versionRef.ID_Object equals minor.ID_Object
                                        join build in buildList on versionRef.ID_Object equals build.ID_Object
                                        join revision in revisionList on versionRef.ID_Object equals revision.ID_Object
                                        join logEntry in loggingManager.LogEntries on versionRef.ID_Object equals logEntry.IdRef
                                        select new VersionItem
                                        {
                                            ID_Version = versionRef.ID_Object,
                                            Name_Version = versionRef.Name_Object,
                                            ID_Logentry = logEntry.IdLogEntry,
                                            Name_Logentry = logEntry.NameLogEntry,
                                            ID_Logstate = logEntry.IdLogState,
                                            Name_Logstate = logEntry.NameLogState,
                                            DateTimeStamp = logEntry.DateTimeStamp,
                                            ID_Attribute_DateTimeStamp = logEntry.IdAttributeDateTimeStamp,
                                            Message = logEntry.Message,
                                            ID_Attribute_Message = logEntry.IdAttributeMessage,
                                            ID_User = logEntry.IdUser,
                                            Name_User = logEntry.NameUser,
                                            ID_Attribute_Major = major.ID_Attribute,
                                            Major = major.Val_Lng,
                                            ID_Attribute_Minor = minor.ID_Attribute,
                                            Minor = minor.Val_Lng,
                                            ID_Attribute_Build = build.ID_Attribute,
                                            Build = build.Val_Lng,
                                            ID_Attribute_Revision = revision.ID_Attribute,
                                            Revision = revision.Val_Lng,
                                            OrderID = versionRef.OrderID
                                        }).ToList();
                            ResultVersions = localConfig.Globals.LState_Success.Clone();
                        }
                    }
                    else
                    {
                        Versions = (from logEntry in loggingManager.LogEntries
                                    where logEntry.IdRef == currentVersion.ID_Version
                                    select new VersionItem
                                    {
                                        ID_Version = currentVersion.ID_Version,
                                        Name_Version = currentVersion.Name_Version,
                                        ID_Logentry = logEntry.IdLogEntry,
                                        Name_Logentry = logEntry.NameLogEntry,
                                        ID_Logstate = logEntry.IdLogState,
                                        Name_Logstate = logEntry.NameLogState,
                                        DateTimeStamp = logEntry.DateTimeStamp,
                                        ID_Attribute_DateTimeStamp = logEntry.IdAttributeDateTimeStamp,
                                        Message = logEntry.Message,
                                        ID_Attribute_Message = logEntry.IdAttributeMessage,
                                        ID_User = logEntry.IdUser,
                                        Name_User = logEntry.NameUser,
                                        ID_Attribute_Major = currentVersion.ID_Attribute_Major,
                                        Major = currentVersion.Major,
                                        ID_Attribute_Minor = currentVersion.ID_Attribute_Minor,
                                        Minor = currentVersion.Minor,
                                        ID_Attribute_Build = currentVersion.ID_Attribute_Build,
                                        Build = currentVersion.Build,
                                        ID_Attribute_Revision = currentVersion.ID_Attribute_Revision,
                                        Revision = currentVersion.Revision,
                                        OrderID = currentVersion.OrderID
                                    }).ToList();
                        ResultVersions = localConfig.Globals.LState_Success.Clone();
                    }
                    
                    
                }
            }
        }
    }
}
