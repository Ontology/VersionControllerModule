﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControllerModule.Models
{
    public class LogStateItem
    {
        [Combobox(FieldType = CellType.String, IsIdField = true, Property = "Id")]
        [Json(Property = "Id")]
        public string IdLogState { get; set; }

        [Combobox(FieldType = CellType.String, Property = "Name")]
        [Json(Property = "Name")]
        public string NameLogState { get; set; }
    }
}
