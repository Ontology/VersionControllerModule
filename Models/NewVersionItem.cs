﻿using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControllerModule.Models
{
    //public class VersionItem : NotifyPropertyChange
    //{
    //    private string idVersion;
    //    [DataViewColumn(Caption = "ID_Version", CellType = CellType.String, IsVisible = false, IsIdField = true)]
    //    public string ID_Version
    //    {
    //        get { return idVersion; }
    //        set
    //        {
    //            idVersion = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_ID_Version);
    //        }
    //    }

    //    private string nameVersion;
    //    [DataViewColumn(Caption = "Version", CellType = CellType.String, IsVisible = true, DisplayOrder = 0)]
    //    public string Name_Version
    //    {
    //        get { return nameVersion; }
    //        set
    //        {
    //            nameVersion = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_Name_Version);
    //        }
    //    }


    //    private string idAttriubetMajor;
    //    [DataViewColumn(Caption = "ID_Attribute_Major", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Attribute_Major
    //    {
    //        get { return idAttriubetMajor; }
    //        set
    //        {
    //            idAttriubetMajor = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_ID_Attribute_Major);
    //        }
    //    }

    //    private long? major;
    //    [DataViewColumn(Caption = "Major", CellType = CellType.Number, IsVisible = true, DisplayOrder = 1)]
    //    public long? Major
    //    {
    //        get { return major; }
    //        set
    //        {
    //            major = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_ID_Attribute_Major);
    //        }
    //    }

    //    private string idAttributeMinor;
    //    [DataViewColumn(Caption = "ID_Attribute_Minor", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Attribute_Minor
    //    {
    //        get { return idAttributeMinor; }
    //        set
    //        {
    //            idAttributeMinor = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_ID_Attribute_Minor);
    //        }
    //    }

    //    private long? minor;
    //    [DataViewColumn(Caption = "Minor", CellType = CellType.Number, IsVisible = true, DisplayOrder = 2)]
    //    public long? Minor
    //    {
    //        get { return minor; }
    //        set
    //        {
    //            minor = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_Minor);
    //        }
    //    }

    //    private string idAttributeBuild;
    //    [DataViewColumn(Caption = "ID_Attribute_Build", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Attribute_Build
    //    {
    //        get { return idAttributeBuild; }
    //        set
    //        {
    //            idAttributeBuild = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_ID_Attribute_Build);
    //        }
    //    }

    //    private long? build;
    //    [DataViewColumn(Caption = "Build", CellType = CellType.Number, IsVisible = true, DisplayOrder = 3)]
    //    public long? Build
    //    {
    //        get { return build; }
    //        set
    //        {
    //            build = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_Build);
    //        }
    //    }

    //    private string idAttributeRevision;
    //    [DataViewColumn(Caption = "ID_Attribute_Revision", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Attribute_Revision
    //    {
    //        get { return idAttributeRevision; }
    //        set
    //        {
    //            idAttributeRevision = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_ID_Attribute_Revision);
    //        }
    //    }

    //    public long? revision;
    //    [DataViewColumn(Caption = "Revision", CellType = CellType.Number, IsVisible = true, DisplayOrder = 4)]
    //    public long? Revision
    //    {
    //        get { return revision; }
    //        set
    //        {
    //            revision = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_Revision);
    //        }
    //    }

    //    public string idLogEntry;
    //    [DataViewColumn(Caption = "ID_Logentry", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Logentry
    //    {
    //        get { return idLogEntry; }
    //        set
    //        {
    //            idLogEntry = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Model_ID_Logentry);
    //        }
    //    }

    //    private string nameLogEntry;
    //    [DataViewColumn(Caption = "Name_Logentry", CellType = CellType.String, IsVisible = false)]
    //    public string Name_Logentry
    //    {
    //        get { return nameLogEntry; }
    //        set
    //        {
    //            nameLogEntry = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.Name_Logentry);
    //        }
    //    }

    //    private string idAttributeDateTimeStamp;
    //    [DataViewColumn(Caption = "ID_Attribute_DateTimeStamp", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Attribute_DateTimeStamp
    //    {
    //        get { return idAttributeDateTimeStamp; }
    //        set
    //        {
    //            idAttributeDateTimeStamp = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.ID_Attribute_DateTimeStamp);
    //        }
    //    }

    //    private DateTime? dateTimeStamp;
    //    [DataViewColumn(Caption = "DateTimeStamp", CellType = CellType.Date, IsVisible = true, DisplayOrder = 5)]
    //    public DateTime? DateTimeStamp
    //    {
    //        get { return dateTimeStamp; }
    //        set
    //        {
    //            dateTimeStamp = value;
    //            RaisePropertyChanged(Notifications.NotifyChanges.DateTimeStamp);
    //        }
    //    }

    //    [DataViewColumn(Caption = "ID_Attribute_Message", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Attribute_Message { get; set; }

    //    [DataViewColumn(Caption = "Message", CellType = CellType.String, IsVisible = true, DisplayOrder = 6)]
    //    public string Message { get; set; }

    //    [DataViewColumn(Caption = "ID_Logstate", CellType = CellType.String, IsVisible = false)]
    //    public string ID_Logstate { get; set; }

    //    [DataViewColumn(Caption = "LogState", CellType = CellType.String, IsVisible = true, DisplayOrder = 7)]
    //    public string Name_Logstate { get; set; }

    //    [DataViewColumn(Caption = "ID_User", CellType = CellType.String, IsVisible = false)]
    //    public string ID_User { get; set; }

    //    [DataViewColumn(Caption = "User", CellType = CellType.String, IsVisible = true, DisplayOrder = 8)]
    //    public string Name_User { get; set; }

    //    [DataViewColumn(Caption = "OrderId", CellType = CellType.Number, IsVisible = true, DisplayOrder = 9)]
    //    public long? OrderID { get; set; }
    //}
}
