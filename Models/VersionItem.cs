﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControllerModule.Models
{
    public class VersionItem
    {
        [DataViewColumn(Caption = "ID_Version", CellType = CellType.String, IsVisible = false, IsIdField = true)]
        public String ID_Version { get; set; }

        [DataViewColumn(Caption = "Version", CellType = CellType.String, IsVisible = true, DisplayOrder = 0)]
        public string Name_Version { get; set; }

        [DataViewColumn(Caption = "ID_Attribute_Major", CellType = CellType.String, IsVisible = false)]
        public string ID_Attribute_Major { get; set; }

        [DataViewColumn(Caption = "Major", CellType = CellType.Number, IsVisible = true, DisplayOrder = 1)]
        public long? Major { get; set; }

        [DataViewColumn(Caption = "ID_Attribute_Minor", CellType = CellType.String, IsVisible = false)]
        public string ID_Attribute_Minor { get; set; }

        [DataViewColumn(Caption = "Minor", CellType = CellType.Number, IsVisible = true, DisplayOrder = 2)]
        public long? Minor { get; set; }

        [DataViewColumn(Caption = "ID_Attribute_Build", CellType = CellType.String, IsVisible = false)]
        public string ID_Attribute_Build { get; set; }

        [DataViewColumn(Caption = "Build", CellType = CellType.Number, IsVisible = true, DisplayOrder = 3)]
        public long? Build { get; set; }

        [DataViewColumn(Caption = "ID_Attribute_Revision", CellType = CellType.String, IsVisible = false)]
        public string ID_Attribute_Revision { get; set; }

        [DataViewColumn(Caption = "Revision", CellType = CellType.Number, IsVisible = true, DisplayOrder = 4)]
        public long? Revision { get; set; }

        [DataViewColumn(Caption = "ID_Logentry", CellType = CellType.String, IsVisible = false)]
        public string ID_Logentry { get; set; }

        [DataViewColumn(Caption = "Name_Logentry", CellType = CellType.String, IsVisible = false)]
        public string Name_Logentry { get; set; }

        [DataViewColumn(Caption = "ID_Attribute_DateTimeStamp", CellType = CellType.String, IsVisible = false)]
        public string ID_Attribute_DateTimeStamp { get; set; }

        [DataViewColumn(Caption = "DateTimeStamp", CellType = CellType.Date, IsVisible = true, DisplayOrder = 5)]
        public DateTime? DateTimeStamp { get; set; }

        [DataViewColumn(Caption = "ID_Attribute_Message", CellType = CellType.String, IsVisible = false)]
        public string ID_Attribute_Message { get; set; }

        [DataViewColumn(Caption = "Message", CellType = CellType.String, IsVisible = true, DisplayOrder = 6)]
        public string Message { get; set; }

        [DataViewColumn(Caption = "ID_Logstate", CellType = CellType.String, IsVisible = false)]
        public string ID_Logstate { get; set; }

        [DataViewColumn(Caption = "LogState", CellType = CellType.String, IsVisible = true, DisplayOrder = 7)]
        public string Name_Logstate { get; set; }

        [DataViewColumn(Caption = "ID_User", CellType = CellType.String, IsVisible = false)]
        public string ID_User { get; set; }

        [DataViewColumn(Caption = "User", CellType = CellType.String, IsVisible = true, DisplayOrder = 8)]
        public string Name_User { get; set; }

        [DataViewColumn(Caption = "OrderId", CellType = CellType.Number, IsVisible = true, DisplayOrder = 9)]
        public long? OrderID { get; set; }
    }
}
