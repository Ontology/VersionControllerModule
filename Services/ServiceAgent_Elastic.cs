﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VersionControllerModule.Models;
using VersionControllerModule.Notifications;

namespace VersionControllerModule.Services
{
    public class ServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private clsOntologyItem oItemRef;

        private Thread getVersionAsync;
        private Thread getLogStatesAsync;

        private OntologyModDBConnector dbReaderVersionsOfRef;
        private OntologyModDBConnector dbReaderVersionNumbers;

        private clsOntologyItem oItemDirection;
        public clsOntologyItem OItemDirection
        {
            get
            {
                return oItemDirection;
            }
        }

        private clsOntologyItem resultVersions;
        public clsOntologyItem ResultVersions
        {
            get { return resultVersions; }
            set
            {
                resultVersions = value;
                RaisePropertyChanged(NotifyChanges.ServiceAgentElastic_ResultVersions);
            }
        }

        private List<clsOntologyItem> logStates;
        public List<clsOntologyItem> LogStates
        {
            get { return logStates; }
            set
            {
                logStates = value;
                RaisePropertyChanged(NotifyChanges.ServiceAgentElastic_LogStates);
            }
        }

        public VersionItem CurrentVersion { get; private set; }
        

        public List<clsObjectRel> VersionsOfRef
        {
            get
            {
                return dbReaderVersionsOfRef.ObjectRels;
            }
        }

        public List<clsObjectAtt> MajorNumbers
        {
            get
            {
                return dbReaderVersionNumbers.ObjAtts.Where(versionNumber => versionNumber.ID_AttributeType == localConfig.OItem_attribute_major.GUID).ToList();
            }
        }

        public List<clsObjectAtt> MinorNumbers
        {
            get
            {
                return dbReaderVersionNumbers.ObjAtts.Where(versionNumber => versionNumber.ID_AttributeType == localConfig.OItem_attribute_minor.GUID).ToList();
            }
        }

        public List<clsObjectAtt> BuildNumbers
        {
            get
            {
                return dbReaderVersionNumbers.ObjAtts.Where(versionNumber => versionNumber.ID_AttributeType == localConfig.OItem_attribute_build.GUID).ToList();
            }
        }

        public List<clsObjectAtt> RevisionNumbers
        {
            get
            {
                return dbReaderVersionNumbers.ObjAtts.Where(versionNumber => versionNumber.ID_AttributeType == localConfig.OItem_attribute_revision.GUID).ToList();
            }
        }

        public clsOntologyItem GetOItem(string guidItem, string type)
        {
            var dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);
            return dbReaderOItem.GetOItem(guidItem, type);
        }

        public ServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderVersionsOfRef = new OntologyModDBConnector(localConfig.Globals);
            dbReaderVersionNumbers = new OntologyModDBConnector(localConfig.Globals);
        }

        public clsOntologyItem GetCurrentVersion(clsOntologyItem oItemRef)
        {
            this.oItemRef = oItemRef;

            if (getVersionAsync != null)
            {
                try
                {
                    getVersionAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }

            getVersionAsync = new Thread(GetCurrentVersionAsync);
            getVersionAsync.Start();


            return localConfig.Globals.LState_Success.Clone();
        }

        public void GetCurrentVersionAsync()
        {
            CurrentVersion = null;
            if (oItemRef == null)
            {
                ResultVersions = localConfig.Globals.LState_Nothing.Clone();
                return;
            }

            var result = localConfig.Globals.LState_Success.Clone();
            if (oItemRef.GUID_Parent != localConfig.OItem_type_developmentversion.GUID)
            {
                result = Get_001_VersionsOfRef();
            }

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultVersions = result;
                return;
            }

            result = Get_002_CurrentVersionNumbers();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultVersions = result;
                return;
            }

            if (oItemRef.GUID == localConfig.OItem_type_developmentversion.GUID)
            {
                CurrentVersion = (from majorNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_major.GUID)
                                  join minorNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_minor.GUID) on majorNumber.ID_Object equals minorNumber.ID_Object
                                  join buildNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_build.GUID) on majorNumber.ID_Object equals buildNumber.ID_Object
                                  join revisionNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_revision.GUID) on majorNumber.ID_Object equals revisionNumber.ID_Object
                                  select new VersionItem
                                  {
                                      ID_Version = oItemRef.GUID,
                                      Name_Version = oItemRef.Name,
                                      ID_Attribute_Major = majorNumber.ID_Attribute,
                                      Major = majorNumber.Val_Lng,
                                      Minor = minorNumber.Val_Lng,
                                      Build = buildNumber.Val_Lng,
                                      Revision = revisionNumber.Val_Lng
                                  }).FirstOrDefault();
            }
            else
            {
                var currentVersions = new List<VersionItem>();

                if (OItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                {
                    currentVersions = (from rel in dbReaderVersionsOfRef.ObjectRels
                                       join majorNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_major.GUID) on rel.ID_Other equals majorNumber.ID_Object
                                       join minorNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_minor.GUID) on rel.ID_Other equals minorNumber.ID_Object
                                       join buildNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_build.GUID) on rel.ID_Other equals buildNumber.ID_Object
                                       join revisionNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_revision.GUID) on rel.ID_Other equals revisionNumber.ID_Object
                                       select new VersionItem
                                       {
                                           ID_Version = rel.ID_Other,
                                           Name_Version = rel.Name_Other,
                                           ID_Attribute_Major = majorNumber.ID_Attribute,
                                           Major = majorNumber.Val_Lng,
                                           Minor = minorNumber.Val_Lng,
                                           Build = buildNumber.Val_Lng,
                                           Revision = revisionNumber.Val_Lng,
                                           OrderID = rel.OrderID
                                       }).ToList();

                    
                }
                else
                {
                    currentVersions = (from rel in dbReaderVersionsOfRef.ObjectRels
                                       join majorNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_major.GUID) on rel.ID_Object equals majorNumber.ID_Object
                                       join minorNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_minor.GUID) on rel.ID_Object equals minorNumber.ID_Object
                                       join buildNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_build.GUID) on rel.ID_Object equals buildNumber.ID_Object
                                       join revisionNumber in dbReaderVersionNumbers.ObjAtts.Where(verNum => verNum.ID_AttributeType == localConfig.OItem_attribute_revision.GUID) on rel.ID_Object equals revisionNumber.ID_Object
                                       select new VersionItem
                                       {
                                           ID_Version = rel.ID_Object,
                                           Name_Version = rel.Name_Object,
                                           ID_Attribute_Major = majorNumber.ID_Attribute,
                                           Major = majorNumber.Val_Lng,
                                           Minor = minorNumber.Val_Lng,
                                           Build = buildNumber.Val_Lng,
                                           Revision = revisionNumber.Val_Lng,
                                           OrderID = rel.OrderID
                                       }).ToList();
                }

                CurrentVersion = currentVersions.OrderByDescending(curVer => curVer.OrderID).OrderByDescending(curVer => curVer.Major).ThenByDescending(curVer => curVer.Minor).ThenByDescending(curVer => curVer.Build).ThenByDescending(curVer => curVer.Revision).FirstOrDefault();

            }

            ResultVersions = localConfig.Globals.LState_Success.Clone();
        }

        private clsOntologyItem Get_002_CurrentVersionNumbers()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var searchNumbers = new List<clsObjectAtt>();
            
            if (oItemRef.GUID_Parent != localConfig.OItem_type_developmentversion.GUID)
            { 
                searchNumbers = dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attribute_major.GUID,
                    ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
                }).ToList();

                searchNumbers.AddRange(dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attribute_minor.GUID,
                    ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
                }));

                searchNumbers.AddRange(dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attribute_build.GUID,
                    ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
                }));

                searchNumbers.AddRange(dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attribute_revision.GUID,
                    ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
                }));

                
            }
            else
            {
                searchNumbers = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = oItemRef.GUID,
                        ID_AttributeType = localConfig.OItem_attribute_major.GUID
                    },
                    new clsObjectAtt
                    {
                        ID_Object = oItemRef.GUID,
                        ID_AttributeType = localConfig.OItem_attribute_minor.GUID
                    },
                    new clsObjectAtt
                    {
                        ID_Object = oItemRef.GUID,
                        ID_AttributeType = localConfig.OItem_attribute_build.GUID
                    },
                    new clsObjectAtt
                    {
                        ID_Object = oItemRef.GUID,
                        ID_AttributeType = localConfig.OItem_attribute_revision.GUID
                    }
                };
            }

            if (searchNumbers.Any())
            {
                result = dbReaderVersionNumbers.GetDataObjectAtt(searchNumbers);

            }
            else
            {
                dbReaderVersionNumbers.ObjAtts.Clear();

                return localConfig.Globals.LState_Success.Clone();
            }


            return result;
        }

        public clsOntologyItem GetLogStates()
        {
            StopReadLogStates();

            getLogStatesAsync = new Thread(GetLogStatesAsync);
            getLogStatesAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        public clsOntologyItem GetVersions(clsOntologyItem oItemRef)
        {
            this.oItemRef = oItemRef;

            StopReadGetVersions();

            getVersionAsync = new Thread(GetVersionsAsync);
            getVersionAsync.Start();


            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetLogStatesAsync()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchLogStates = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_type_logstate.GUID
                }
            };

            var result = dbReader.GetDataObjects(searchLogStates);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                LogStates = dbReader.Objects1;
            }
            else
            {
                LogStates = null;
            }
        }

        public void StopRead()
        {
            StopReadGetVersions();
            StopReadLogStates();
        }

        public void StopReadGetVersions()
        {
            if (getVersionAsync != null)
            {
                try
                {
                    getVersionAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
            
        }

        public void StopReadLogStates()
        {
            if (getLogStatesAsync != null)
            {
                try
                {
                    getLogStatesAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void GetVersionsAsync()
        {
            var result = Get_001_VersionsOfRef();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = Get_002_VersionNumbersOfRef();
            }
            ResultVersions = result;
        }

        private clsOntologyItem Get_001_VersionsOfRef()
        {
            var searchRefOfItem = new List<clsObjectRel>();

            searchRefOfItem.Add(new clsObjectRel
            {
                ID_Object = oItemRef.GUID,
                ID_Parent_Other = localConfig.OItem_type_developmentversion.GUID
            });

            var result = dbReaderVersionsOfRef.GetDataObjectRel(searchRefOfItem);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            if (dbReaderVersionsOfRef.ObjectRels.Any())
            {
                oItemDirection = localConfig.Globals.Direction_LeftRight.Clone();
                return result;
            }

            
            searchRefOfItem.Add(new clsObjectRel
            {
                ID_Other = oItemRef.GUID,
                ID_Parent_Object = localConfig.OItem_type_developmentversion.GUID
            });

            oItemDirection = localConfig.Globals.Direction_RightLeft.Clone();
            result = dbReaderVersionsOfRef.GetDataObjectRel(searchRefOfItem);


            return result;
        }

        public clsOntologyItem Get_002_VersionNumbersOfRef()
        {

            var searchNumbers = dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
            {
                ID_AttributeType = localConfig.OItem_attribute_major.GUID,
                ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
            }).ToList();

            searchNumbers.AddRange(dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
            {
                ID_AttributeType = localConfig.OItem_attribute_minor.GUID,
                ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
            }));

            searchNumbers.AddRange(dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
            {
                ID_AttributeType = localConfig.OItem_attribute_build.GUID,
                ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
            }));

            searchNumbers.AddRange(dbReaderVersionsOfRef.ObjectRels.Select(rel => new clsObjectAtt
            {
                ID_AttributeType = localConfig.OItem_attribute_revision.GUID,
                ID_Object = oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object
            }));
            
            if (searchNumbers.Any())
            {
                var result = dbReaderVersionNumbers.GetDataObjectAtt(searchNumbers);
                return result;
            }
            else
            {
                dbReaderVersionNumbers.ObjAtts.Clear();

                return localConfig.Globals.LState_Success.Clone();
            }
        }
    }
}
